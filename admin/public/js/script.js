$(document).ready(function() {

// Member Register //

		$(".error").hide();

		$(".telephone").keypress(function (e) {
		    if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
		});

		$(".postal_code").keypress(function (e) {
		    if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
		});

		$("#submitDesktopRegister").click(function(){
			$('#register-form').submit();
		});

		$('#register-form').validate({

			ignore: [],
			rules: {
				password 		:
				{
					required	: true,
					minlength	: 6,
					maxlength	: 16
				},
			    confirm_password: 
			    {
			      	equalTo 	: "#password"
			    },
				firstname 		: "required",
				lastname     	: "required",
				email 			: {
					required	: true,
					email 		: true,
					remote		:
                    {
                      	url 	: site_url+"member/check_email_register",
                      	type	: "post"
                    }
				},
				day				: "required",
				month			: "required",
				year			: "required",
				is_accept		: "required"
			},
			//Error messages
			messages: {

				password		: 
				{
					required	: register_password_required,
					minlength	: register_password_minlength,
					maxlength	: register_password_maxlength
				},
				confirm_password:
				{
					equalTo		: register_confirm_password_equalTo
				},
				firstname 		: register_firstname_required,
				lastname		: register_lastname_required,
				is_accept 		: register_is_accept_required,
				email			: 
				{
					required	: register_email_required,
					email 		: register_email_email,
                    remote 		: register_email_remote
				},
				day				: register_day_required,
				month			: register_month_required,
				year			: register_year_required
			}
		});

// Member Signin //

		$("#submitDesktopSignin").click(function(){
			$('#signin-form').submit();
		});

		$('#signin-form').validate({

			rules: {
				email: {
					required 	: true,
					email 		: true
				},
				password: "required"
			},
			messages: {
				email: 
				{
					required	: register_email_required,
					email 		: register_email_email
				},
				password: register_password_required
			}
		});

// Member Reset Password

		$("#submitDesktopReset").click(function(){
			$('#reset-password-form').submit();
		});

		$('#reset-password-form').validate({

			rules: {
				email_reset 	:
				{
					required	: true,
					email 		: true
				}
			},
			messages: {
				email_reset		: 
				{
					required	: register_email_required,
					email 		: register_email_email
				}
			}
		});

// Member Profile //

		$("#submitProfile").click(function() {

			$('#profile-form').validate({

				ignore: [],
				rules: {
					firstname 		: "required",
					lastname     	: "required",
					telephone 		:
					{
						required	: true,
						minlength	: 9,
						maxlength	: 10
					},
					email 			: {
						required	: true,
						email 		: true,
						remote		:
	                    {
	                      	url 	: site_url+"member/check_email_register",
	                      	type	: "post"
	                    }
					},
					day				: "required",
					month			: "required",
					year			: "required",
					gender			: "required"
				},
				//Error messages
				messages: {

					firstname 		: register_firstname_required,
					lastname		: register_lastname_required,
					telephone 		: 
					{
						required	: register_telephone_required,
						minlength	: register_telephone_minlength,
						maxlength	: register_telephone_maxlength
					},
					email			: 
					{
						required	: register_email_required,
						email 		: register_email_email,
	                    remote 		: register_email_remote
					},
					day				: register_day_required,
					month			: register_month_required,
					year			: register_year_required,
					gender			: register_gender_required
				}
			});

			$('#profile-form').submit();
		});

// Member Address

		$("#add_address").click(function() {

		    $('#address-form').validate({

		      ignore: [],
		      rules: {
		        postal_code   : {
		          required  : true,
		          number    : true,
		          minlength : 5, 
		          maxlength : 5
		        },
		        address_no    : "required",
		        address_soi   : "required",
		        address_road  : "required",
		        province_id   :
		        {
		          required  : true
		        },
		        district_id   :
		        {
		          required  : true
		        },
		        sub_district_id :
		        {
		          required  : true
		        }
		      },
		      //Error messages
		      messages: {

		        address_no    : register_address_no_required,
		        address_soi   : register_address_soi_required,
		        address_road  : register_address_road_required,
		        province_id   : register_province_required,
		        district_id   : register_district_required,
		        sub_district_id : register_sub_district_required,
		        postal_code   :
		        {
		          required  : register_postal_code_required,
		          minlength : register_postal_code_minlength
		        }
		      }
		    });

		    $('#address-form').submit();
		});

// Member Change Password //

		$("#change_password").click(function() {

			$('#password-form').validate({
				rules: {
					old_password 		: "required",
					new_password 		:
					{
						required	: true,
						minlength	: 6,
						maxlength	: 16
					},
					confirm_new_password 	: {
						equalTo 	: "#new_password"
					}
				},
				//Error messages
				messages: {
					old_password 		: register_password_required,
					new_password		: 
					{
						required	: register_password_required,
						minlength	: register_password_minlength,
						maxlength	: register_password_maxlength
					},
					confirm_new_password	: 
					{
						equalTo 	: register_confirm_password_equalTo
					}
				}
			});

			$('#password-form').submit();
		});

// Contact Mail //

		$("#contactSubmit").click(function() {

			$('#contact-form').validate({
				rules: {
					topic 			: "required",
					fullname 		:
					{
						required	: true
					},
					telephone 		: {
						number		: true,
						minlength	: 10,
						maxlength	: 10
					},
					email 			: {
						email 		: true,
						required	: true
					},
					detail			: "required"
				},
				//Error messages
				messages: {
					topic 			: contact_topic_required,
					fullname		: 
					{
						required	: contact_fullname_required
					},
					telephone		: 
					{
						required	: register_telephone_required,
					},
					email			: 
					{
						required	: register_email_required,
						email 		: register_email_email
					},
					detail 			: contactus_form_detail,

				}
			});

			$('#contact-form').submit();
		});

// Check out Guest

		$("#submitCheckout").click(function() {

			$('#checkout-form-guest').validate({

				ignore: [],
				rules: {
					firstname 		: "required",
					lastname     	: "required",
					postal_code 	: {
						required	: true,
						number		: true,
						minlength	: 5, 
						maxlength	: 5
					},
					email 			: {
						required	: true,
						email 		: true,
						remote 		: {
	                      	url 	: site_url+"member/check_email_register",
	                      	type	: "post"
	                    }
					},
					telephone 		: {
						required 	: true,
						number 		: true,
						minlength	: 9, 
						maxlength	: 10
					},
					address_no 		: "required",
					province_id 	:
					{
						required	: true
					},
					district_id 	:
					{
						required	: true
					},
					sub_district_id :
					{
						required	: true
					},
					is_accept		: "required",
					tax_id 			:
					{
						minlength	: 13,
						maxlength	: 15
					}
				},
				//Error messages
				messages: {

					firstname 		: register_firstname_required,
					lastname		: register_lastname_required,
					address_no 		: register_address_no_required,
					province_id 	: register_province_required,
					district_id 	: register_district_required,
					sub_district_id : register_sub_district_required,
					postal_code 	:
					{
						required	: register_postal_code_required,
						minlength	: register_postal_code_minlength
					},
					is_accept 		: register_is_accept_required,
					email			: 
					{
						required	: register_email_required,
						email 		: register_email_email,
						remote 		: register_email_remote
					},
					telephone 		:
					{
						required 	: register_telephone_required,
						minlength 	: register_telephone_minlength,
						maxlength 	: register_telephone_maxlength
					},
					tax_id 			:
					{
						minlength 	: register_tax_id_minlength,
						maxlength 	: register_tax_id_maxlength
					}
				}
			});

            $('#province_id').prop('disabled',false);

            $('#district_id').prop('disabled',false);

            $('#sub_district_id').prop('disabled',false);

			$('#checkout-form-guest').submit();

		});

// Check out Member

		$("#submitCheckout").click(function() {

			$('#checkout-form-member').validate({

				ignore: [],
				rules: {
					firstname 		: "required",
					lastname     	: "required",
					postal_code 	: {
						required	: true,
						number		: true,
						minlength	: 5, 
						maxlength	: 5
					},
					email 			: {
						required	: true,
						email 		: true
					},
					telephone 		: {
						required 	: true,
						number 		: true,
						minlength	: 9, 
						maxlength	: 10
					},
					address_no 		: "required",
					province_id 	:
					{
						required	: true
					},
					district_id 	:
					{
						required	: true
					},
					sub_district_id :
					{
						required	: true
					},
					is_accept		: "required",
					tax_id 			:
					{
						minlength	: 13,
						maxlength	: 15
					}
				},
				//Error messages
				messages: {

					firstname 		: register_firstname_required,
					lastname		: register_lastname_required,
					address_no 		: register_address_no_required,
					province_id 	: register_province_required,
					district_id 	: register_district_required,
					sub_district_id : register_sub_district_required,
					postal_code 	:
					{
						required	: register_postal_code_required,
						minlength	: register_postal_code_minlength
					},
					is_accept 		: register_is_accept_required,
					email			: 
					{
						required	: register_email_required,
						email 		: register_email_email
					},
					telephone 		:
					{
						required 	: register_telephone_required,
						minlength 	: register_telephone_minlength,
						maxlength 	: register_telephone_maxlength
					},
					tax_id 			:
					{
						minlength 	: register_tax_id_minlength,
						maxlength 	: register_tax_id_maxlength
					}
				}
			});

            $('#province_id').prop('disabled',false);

            $('#district_id').prop('disabled',false);

            $('#sub_district_id').prop('disabled',false);

			$('#checkout-form-member').submit();

		});

// Search
		$("#submitDesktopSearch").click(function(){
			$('#search-form').submit();
		});

		$('input[name=coupon]').on('blur', function(){
			var coupon = $('input[name=coupon]').val();

			if(coupon == 'thaiunionstaff') {
				$('tr.thaiunionstaff').show();
			}
			else {
				$('tr.thaiunionstaff').hide();
			}
	    });

	    $('input[name=coupon]').keyup(function() {
		    delay(function(){
		      var item = [];
				var coupon = $('input[name=coupon]').val();
				if(coupon == 'thaiunionstaff') {
					$('tr.thaiunionstaff').show();
				}
				else {
					$('tr.thaiunionstaff').hide();
				}
		    }, 1000);
		});

		var delay = (function(){
			var timer = 0;
			return function(callback, ms){
				clearTimeout (timer);
				timer = setTimeout(callback, ms);
			};
		})();
});