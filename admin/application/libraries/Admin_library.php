<?php
class Admin_library {
	private $ci;
	private $setting = array();
	private $admin_session_id = "";
	private $admin_session= array();
	private $breadcrumb= array();
	private $admin_islogin = false;
	private $admin_issuper = NULL;
	public function __construct()
	{
		@ob_start();
		$this->ci = & get_instance();
		$this->setting['body_entry']="";
		$this->setting['get']=array();
		$this->setting['title'] = "Administrator";
		$this->setting['asset_url'] = base_url("public")."/";
		$this->setting['site_url'] = site_url();
		$this->setting['base_url'] = base_url();
		$this->setting['admin_url'] = admin_url();
		$this->setting['current_url'] = site_url($this->ci->uri->uri_string());
		$this->setting['navi_title'] = NULL;
		$this->setting['navi_icon'] = NULL;
		$this->setting['toolbar'] = array();
		$this->setting['company_name']=NULL;

		$this->admin_session_id = "admss_" . md5($this->ci->input->server("HTTP_HOST"));
		$this->admin_session = $this->ci->session->userdata($this->admin_session_id);

		if($this->admin_session) {
			$this->admin_session=json_decode(base64_decode($this->admin_session),true);
			$this->admin_islogin=true;
			$this->_getuserinfo();
			$this->getCompanyName();
		}
		if(strpos($_SERVER['HTTP_USER_AGENT'],"iPhone") !== false || strpos($_SERVER['HTTP_USER_AGENT'],"Android") !== false) {
			$this->setting['desktop']=false;
			$this->setting['mobile']=true;
			$this->setting['tablet']=false;
		} else if(strpos($_SERVER['HTTP_USER_AGENT'],"iPad") !== false) {
			$this->setting['desktop']=false;
			$this->setting['mobile']=false;
			$this->setting['tablet']=true;
		} else {
			$this->setting['desktop']=true;
			$this->setting['mobile']=false;
			$this->setting['tablet']=false;
		}
	}
	public function userdata($name)
	{
		if(!$this->admin_session) {
			return NULL;
		}
		if(!isset($this->admin_session[$name])) {
			return NULL;
		}

		return $this->admin_session[$name];
	}
	public function set_userdata($name,$value,$session_login)
	{
		$this->admin_session[$name]=$value;
		$setdata = base64_encode(json_encode($this->admin_session));
		$session_time = ( $session_login=="Y" ? ( 60*60*24*365*2 ) : "7200" );

		$data_session = array(
							$this->admin_session_id => $setdata,
							"sess_expiration" => $session_time,
							"sess_id" => $this->ci->session->session_id
						);

		$this->ci->session->set_userdata($data_session);
	}
	public function setLogin($user_id, $session_login)
	{
		$this->set_userdata('user_id',$user_id, $session_login);
	}
	public function logout()
	{
		$this->ci->session->unset_userdata($this->admin_session_id);
	}
	public function isLogin()
	{
		return $this->admin_islogin;
	}
	public function forceLogin()
	{
		if($this->isLogin()==false) {
			admin_redirect("login?next=" . $this->ci->uri->uri_string());
		}
	}
	public function assign($name,$value)
	{
		$this->setting[$name] = $value;
	}
	public function setTitle($title,$icon=NULL)
	{
		$this->setting['navi_icon'] = $icon;
		$this->setting['navi_title'] = $title;
		$this->setting['title'] = $title . " Administrator";
	}
	public function setDetail($detail)
	{
		$this->setting['page_detail'] = $detail;
	}
	public function addToolbar($view)
	{
		$this->setting['toolbar'][] = $this->ci->parser->parse($view,$this->setting,true);
	}
	public function addToolbarLink($label,$link,$icon)
	{
		$html = "";
		if($icon){
			$html .= "<i class=\"{$icon} icon-large\"></i>";	
		}
		$html .= "<a href=\"{$link}\">{$label}</a>";
		$this->setting['toolbar'][]=$html;
	}
	public function output($uri=null)
	{
		// Check user permistion
		if($this->is_superadmin()==false) {

			$menu_checked			= false;
			$submenu_checked		= false;
		} else {
			// Allow all menu
			$menu_checked			= true;
			$submenu_checked		= true;
		}

		$accept_menu 			= array();
		$accept_submenu 		= array();
		$total_url 				= $this->ci->uri->total_segments();
		$current_url			= $this->ci->uri->segment_array();
		$menu_permission_all 	= $this->_menu_entry();

		foreach($menu_permission_all as $rs) {

			if(count($rs['submenu_entry'])!=0) {

				foreach($rs['submenu_entry'] as $submenu) {

						$accept_submenu[] = $submenu['url'];
				}

				$accept_menu[] = $rs['url'];

			} else {

				$accept_menu[] = $rs['url'];
			}
		}

		$accept_menu[] 		= "syssetting";
		$accept_submenu[] 	= "useradd";
		$accept_submenu[] 	= "useredit";
		$accept_submenu[] 	= "usergroupadd";
		$accept_submenu[] 	= "usergroupedit";
		$accept_submenu[] 	= "index";

		if( in_array($current_url[1], $accept_menu) ) {

			$menu_checked = true;

			if($current_url[1] == "syssetting") {

				if( in_array($current_url[2], $accept_submenu) ) {

					$submenu_checked = true;
				}
			} else if( $current_url[1] == "dashboard" ) {

				if(count($current_url) > 1) {

					if( in_array($current_url[2], $accept_submenu) ) {

						$submenu_checked = true;
					}
				} else {

					$submenu_checked = true;
				}

			} else {

				$submenu_checked = true;
			}
		}

		if( $menu_checked && $submenu_checked ) {

			$this->setting['menu_entry'] = $this->_menu_entry();
			$this->setting['header_bar'] = $this->ci->parser->parse("conquer/header_bar",$this->setting,true);
			$this->setting['page_navi'] = $this->ci->parser->parse("conquer/page_navi",$this->setting,true);
			$this->setting['left_menu'] = $this->ci->parser->parse("conquer/left_menu",$this->setting,true);

			$check_cache = $this->ci->cache_library->get($uri);

			if ($check_cache === false) {

				$this->ci->load->view("conquer/template",$this->setting);
				$html['html'] = $this->ci->load->view("conquer/template",$this->setting,true);

				$this->ci->cache_library->write($html, $uri, 2);
			} else {

				echo $this->ci->cache_library->get($uri);
			}

		} else {

			admin_redirect('dashboard/index');
		}
	}
	public function loginoutput($uri=null)
	{
		$check_cache = $this->ci->cache_library->get($uri);

		if ($check_cache === false) {

			$this->setting['body_entry'] = @ob_get_contents();
			@ob_end_clean();
			$this->ci->load->view("conquer/login",$this->setting);
			$html['html'] = $this->ci->load->view("conquer/login",$this->setting,true);

			$this->ci->cache_library->write($html, $uri, 2);
		} else {

			echo $this->ci->cache_library->get($uri);
		}
	}
	public function resetpassoutput()
	{
		$this->setting['body_entry'] = @ob_get_contents();
		@ob_end_clean();
		$this->ci->load->view("conquer/resetpass",$this->setting);
	}
	public function view($file,$data=array(),$output=false)
	{
		if($data) {
			$data = array_merge($this->setting,$data);
		} else {
			$data = $this->setting;
		}
		foreach($_GET as $key=>$val) {
			$this->setting['get'][$key]=$val;
		}
		$res= $this->ci->load->view("views/".$file,$data,true);
		if($output==false) {
			$this->setting['body_entry'] = $res;
		}

		return $res;
	}
	public function template($file,$data=array(),$output=false)
	{
		if($data) {
			$data = array_merge($this->setting,$data);
		} else {
			$data = $this->setting;
		}
		foreach($_GET as $key=>$val) {
			$this->setting['get'][$key]=$val;
		}
		$res= $this->ci->load->view("template/".$file,$data,true);
		if($output==false) {
			$this->setting['body_entry'] = $res;
		}

		return $res;
	}
	public function auth($username,$password)
	{
		$password=md5($password);
		$this->ci->db->where("username",$username);
		$this->ci->db->where("password",$password);
		$this->ci->db->where("user_status","active");

		return $this->ci->db->get("system_users")->row_array();
	}
	public function getuserinfo_byemail($email)
	{
		$this->ci->db->where("user_email",$email);
		return $this->ci->db->get("system_users")->row_array();
	}
	public function getuserinfo_byusername($email)
	{
		$this->ci->db->where("username",$email);
		return $this->ci->db->get("system_users")->row_array();
	}
	public function getuserinfo($user_id)
	{
		$this->ci->db->where("user_id",$user_id);
		return $this->ci->db->get("system_users")->row_array();
	}
	public function check_username($username)
	{
		$this->ci->db->where("username",$username);
		return $this->ci->db->count_all_results("system_users");
	}
	public function check_email($user_email)
	{
		$this->ci->db->where("user_email",$user_email);
		return $this->ci->db->count_all_results("system_users");
	}
	// ################### User Setting ################### //
	public function adduser(array $data = array())
	{
		$this->ci->db->set("user_joindate","NOW()",false);
		$this->ci->db->set("user_joinip",$this->ci->input->ip_address());

		return $this->ci->db->insert("system_users", $data);
	}
	public function updateuserinfo(array $data = array())
	{
		$this->ci->db->where("user_id",$data['user_id']);

		return $this->ci->db->update("system_users", $data);
	}
	public function update_status_account(array $data = array())
	{
		$this->ci->db->set("user_status", $data['user_status']);
		$this->ci->db->where_in("user_id", $data['user_id']);

		return $this->ci->db->update("system_users");
	}
	public function updatePassword($user_id,$newpassword)
	{
		$newpassword=md5($newpassword);
		$this->ci->db->set("password",$newpassword);
		$this->ci->db->where("user_id",$user_id);
		$this->ci->db->update("system_users");
		$this->ci->db->set("reset_key","UUID()",false);
		$this->ci->db->set("user_id", $user_id);
		$this->ci->db->set("reset_status","complete");
		$this->ci->db->set("reset_by", $this->ci->admin_library->userdata('user_id'));
		$this->ci->db->set("reset_date","NOW()",false);
		$this->ci->db->set("reset_ip", $this->ci->input->ip_address());

		$this->ci->db->insert("system_resetpass");
	}
	// ################### Group Setting ################### //
	public function getAllGroup($status=null)
	{
		$this->ci->db->order_by("group_name","ASC");
		if(!$status) {
			$this->ci->db->where("group_status","active");
		} else {
			$this->ci->db->where("group_status !=","deleted");
		}

		return $this->ci->db->get("system_group");
	}
	public function getGroupDetail($group_id)
	{
		$this->ci->db->where("group_id",$group_id);
		$this->ci->db->where("group_status !=","deleted");

		return $this->ci->db->get("system_group")->row_array();
	}
	public function addGroup(array $data = array())
	{
		$this->ci->db->insert("system_group", $data);

		return $this->ci->db->insert_id();
	}
	public function updateGroup(array $data = array())
	{
		$this->ci->db->where("group_id",$data['group_id']);

		return $this->ci->db->update("system_group", $data);
	}
	public function clearPermision($group_id)
	{
		$this->ci->db->where("group_id",$group_id);

		return $this->ci->db->delete("system_permision");
	}
	public function addPermision($group_id,$menu_id,$submenu_id)
	{
		if($submenu_id > 0) {
				$this->ci->db->where("menu_id",$menu_id);
				$has = $this->ci->db->count_all_results("system_permision");
				if(!$has) {
					$this->addPermision($group_id,$menu_id,0);
				}
		}
		$this->ci->db->set("submenu_id",$submenu_id);
		$this->ci->db->set("menu_id",$menu_id);
		$this->ci->db->set("group_id",$group_id);

		return $this->ci->db->insert("system_permision");
	}
	public function update_status_group(array $data = array())
	{
		$this->ci->db->set("group_status", $data['group_status']);
		$this->ci->db->where_in("group_id", $data['group_id']);

		return $this->ci->db->update("system_group");
	}
	public function getAllCompany()
	{
		$this->ci->db->order_by("company_name","ASC");
		$this->ci->db->where("company_status !=","deleted");
		return $this->ci->db->get("system_company");
	}
	public function getAllUser()
	{
		$current_admin = $this->_getuserinfo();
		$company_id = $current_admin['company_id'];
		$this->ci->db->select("system_users.user_id,system_users.username,system_users.user_fullname,system_users.user_email,system_users.user_mobileno,system_users.user_status,system_users.user_joindate,system_users.user_group,system_users.user_thumbnail,system_group.company_id");
		$this->ci->db->join("system_group","system_group.group_id=system_users.user_group");
		$this->ci->db->where("system_users.user_status !=","deleted");
		$this->ci->db->where("system_group.company_id",$company_id);

		return $this->ci->db->get("system_users");
	}
	public function getCompanyName()
	{
		if($this->setting['company_name']==NULL) {
			$grp = $this->getGroupDetail($this->setting['user_info']['user_group']);
			$user_company = $grp['company_id'];

			$this->ci->db->select("company_name");
			$this->ci->db->where("company_id",$user_company);
			$rs = $this->ci->db->get("system_company")->row_array();
			if(!$rs) {
				show_error("Cannot find company for current user.");
			}
			$this->setting['company_name'] = $rs['company_name'];
		}

		return $this->setting['company_name'];
	}
	private function _getuserinfo()
	{
		$user_id = $this->userdata("user_id");
		// Select of more detail about user.
		$this->ci->db->select("system_users.user_id,system_users.username,system_users.user_fullname,system_users.user_email,system_users.user_mobileno,system_users.user_status,system_users.user_joindate,system_users.user_group,system_users.user_thumbnail,system_group.company_id, system_group.group_superadmin");
		$this->ci->db->join("system_group","system_group.group_id=system_users.user_group");
		$this->ci->db->where("system_users.user_id",$user_id);
		$this->setting['user_info'] = $this->ci->db->get("system_users")->row_array();

		if($this->setting['user_info']) {
			$grp = $this->getGroupDetail($this->setting['user_info']['user_group']);
			$this->setting['user_company']= $grp['company_id'];
			if(!$this->setting['user_info']) {
				$this->admin_islogin=false;
				return false;
			}
			return $this->setting['user_info'];
		} else {
			$this->admin_islogin=false;
			return false;
		}
	}
	public function getMenu($menu_link)
	{
		$this->ci->db->where("menu_link",$menu_link);

		return $this->ci->db->get("system_menu")->row_array();
	}
	public function getSubMenu($menu_link,$submenu_link)
	{
		$this->ci->db->like("menu_link",$menu_link,"after");
		$menu = $this->ci->db->get("system_menu")->row_array();
		if(!$menu) {
			return false;
		}

		$this->ci->db->where("menu_id",$menu["menu_id"]);
		$this->ci->db->like("menu_link",$submenu_link,"before");
		$submenu = $this->ci->db->get("system_submenu")->row_array();

		return $submenu;
	}
	public function menuPermision($group_id,$menu_id,$submenu_id)
	{
		$this->ci->db->where("group_id",$group_id);
		$this->ci->db->where("menu_id",$menu_id);
		$this->ci->db->where("submenu_id",$submenu_id);
		$has = $this->ci->db->count_all_results("system_permision");
		$has = ($has > 0)?true:false;

		return $has;
	}
	public function add_breadcrumb($menu_label,$menu_link,$menu_icon)
	{
		$this->breadcrumb[] = array(
				"menu_label" => $menu_label,
				"menu_link" => admin_url($menu_link),
				"menu_icon" => $menu_icon,
			);
	}
	public function breadcrumb()
	{
		$result = array();
		$current_class = $this->ci->router->fetch_class();
		$current_method = $this->ci->router->fetch_method();
		$menu = $this->getMenu($current_class);
		$submenu = $this->getSubMenu($current_class,$current_method);
		if($menu) {
			$result[]=array(
				"menu_label" => $menu['menu_label'],
				"menu_link" => admin_url($menu['menu_link']),
				"menu_icon" => $menu['menu_icon'],
			);
		}
		if($submenu) {
			$result[]=array(
				"menu_label" => $submenu['menu_label'],
				"menu_link" => admin_url($menu['menu_link'] . "/" . $submenu['menu_link']),
				"menu_icon" => $submenu['menu_icon'],
			);
		}
		$result = array_merge($result,$this->breadcrumb);
		return $result;
	}
	public function getAllMenu()
	{
		return $this->_admin_menu_entry();
	}
	private function _admin_menu_entry()
	{
		$this->ci->db->order_by("menu_sequent","asc");
		$menu = $this->ci->db->get("system_menu");
		$entry = array();
		foreach($menu->result_array() as $row) {

			$sub_entry = array();
			$sub_entry['submenu_entry']=$this->_admin_sub_menu_entry($row);
			$sub_entry['id'] = $row['menu_id'];
			$sub_entry['label'] = $row['menu_label'];
			$sub_entry['icon'] = $row['menu_icon'];
			$sub_entry['link'] = admin_url($row['menu_link']);
			$class_menu = explode("/", $row['menu_link']);
			$current_class = $this->ci->router->fetch_class();
			$sub_entry['active'] = ($current_class==$class_menu[0])?"active":"";
			$entry[] = $sub_entry;
			unset($sub_entry);
		}
		return $entry;
	}
	private function _admin_sub_menu_entry($menu)
	{
		$this->ci->db->where("menu_id",$menu['menu_id']);
		$this->ci->db->order_by("menu_sequent","asc");
		$submenu = $this->ci->db->get("system_submenu");
		$entry = array();
		foreach($submenu->result_array() as $row) {

			$sub_entry = array();
			$sub_entry['submenu_entry']=array();
			$sub_entry['id'] = $row['submenu_id'];
			$sub_entry['label'] = $row['menu_label'];
			$sub_entry['icon'] = $row['menu_icon'];
			$sub_entry['link'] = admin_url($menu['menu_link'] . "/".$row['menu_link']);
			$class_menu = explode("/", $row['menu_link']);
			$current_class = $this->ci->router->fetch_class();
			$sub_entry['active'] = "";
			$entry[] = $sub_entry;
			unset($sub_entry);
		}
		return $entry;
	}
	function is_superadmin()
	{
		if($this->admin_issuper==NULL) {
			$rs = $this->getGroupDetail($this->setting['user_info']['user_group']);
			if($rs['group_superadmin']=="no") {
				return false;
			} else {
				return true;
			}
		}
		return $this->admin_issuper;
	}
	private function _menu_entry()
	{
		$this->ci->db->where("menu_status !=","deleted");
		$this->ci->db->order_by("menu_sequent","asc");
		$menu = $this->ci->db->get("system_menu");
		$entry = array();
		foreach($menu->result_array() as $row) {

			if($this->is_superadmin()==false) {
				$checked = $this->menuPermision($this->setting['user_info']['user_group'],$row['menu_id'],0);
			} else {
				$checked = true;
			}

			if($checked) {
				$sub_entry = array();
				$sub_entry['submenu_entry']=$this->_sub_menu_entry($row);
				$sub_entry['id'] 	= $row['menu_id'];
				$sub_entry['label'] = $row['menu_label'];
				$sub_entry['icon'] 	= $row['menu_icon'];
				$sub_entry['link'] 	= admin_url($row['menu_seo']);
				$sub_entry['url']	= $row['menu_link'];
				$sub_entry['sequent']	= $row['menu_sequent'];
				$class_menu = explode("/", $row['menu_link']);
				$current_class = $this->ci->router->fetch_class();
				$sub_entry['active'] = ($current_class==$class_menu[0])?"active":"";
				$entry[] = $sub_entry;
				unset($sub_entry);
			}
		}

		return $entry;
	}
	private function _sub_menu_entry($menu)
	{
		$this->ci->db->where("menu_id",$menu['menu_id']);
		$this->ci->db->where("menu_status !=","deleted");
		$this->ci->db->order_by("menu_sequent","asc");
		$submenu = $this->ci->db->get("system_submenu");
		$entry = array();
		foreach($submenu->result_array() as $row) {

			if($this->is_superadmin()==false) {
				$checked = $this->menuPermision($this->setting['user_info']['user_group'],$menu['menu_id'],$row['submenu_id']);
			} else {
				$checked = true;
			}

			if($checked) {
				$sub_entry = array();
				$sub_entry['submenu_entry']=array();
				$sub_entry['id'] 		= $row['submenu_id'];
				$sub_entry['label'] 	= $row['menu_label'];
				$sub_entry['icon'] 		= $row['menu_icon'];
				$sub_entry['link'] 		= admin_url($menu['menu_link'] . "/".$row['menu_link']);
				$sub_entry['url']		= $row['menu_link'];
				$sub_entry['noti']		= $row['menu_notification'];
				$sub_entry['column']	= $row['menu_column'];
				$sub_entry['database']	= $row['menu_database'];
				$class_menu 			= $this->ci->uri->segment(1)."/".$row['menu_link'];
				$current_class 			= $this->ci->uri->uri_string();
				$sub_entry['active'] 	= ($current_class==$class_menu)?"active":"";
				$entry[] 				= $sub_entry;
				unset($sub_entry);
			}
		}

		return $entry;
	}
	public function getLanguageList()
	{
		$result = $this->ci->db->where('is_deleted', 0)->get("ml_languages")->result_array();
		$language = [];
		foreach ($result as $key => $value) {

			$language[$key]['lang_name'] 	= $value['name'];
			$language[$key]['lang_flag'] 	= $value['language_code'].".png";
			$language[$key]['lang_id'] 		= $value['language_code'];
		}

		return $language;
	}
	public function getLanguagename($lang_id)
	{
		$this->ci->db->where("language_code",$lang_id);
		$lang = $this->ci->db->where('is_deleted', 0)->get("ml_languages")->row_array();

		$language['lang_name'] 	= $lang['name'];
		$language['lang_flag'] 	= $lang['language_code'].".png";
		$language['lang_id'] 	= $lang['language_code'];

		return $language['lang_name'];
	}
	public function getLanguageflag($lang_id)
	{
		$this->ci->db->where("language_code",$lang_id);
		$lang = $this->ci->db->where('is_deleted', 0)->get("ml_languages")->row_array();

		$language['lang_name'] 	= $lang['name'];
		$language['lang_flag'] 	= $lang['language_code'].".png";
		$language['lang_id'] 	= $lang['language_code'];

		return $language['lang_flag'];
	}
	public function getUser($user_id)
	{
		$this->ci->db->where("user_id",$user_id);
		$user = $this->ci->db->get("system_users")->row_array();

		return $user['user_fullname'];
	}
	public function getDistrict($district_id)
	{
		if($district_id != "0") {

			$this->ci->db->where("district_id",$district_id);
			$district = $this->ci->db->get("system_district")->row_array();

			return $district['district_name'];
		} else {

			return $district['district_name'] = "-";
		}
	}
	public function getAmphur($amphur_id)
	{
		if($amphur_id != "0") {
			$this->ci->db->where("amphur_id",$amphur_id);
			$amphur = $this->ci->db->get("system_amphur")->row_array();

			return $amphur['amphur_name'];
		} else {

			return $amphur['amphur_name'] = "-";
		}
	}
	public function getProvince($province_id)
	{
		if($province_id != "0") {
			$this->ci->db->where("province_id",$province_id);
			$province = $this->ci->db->get("system_province")->row_array();

			return $province['province_name_th'];
		} else {

			return $province['province_name_th'] = "-";
		}
	}
}