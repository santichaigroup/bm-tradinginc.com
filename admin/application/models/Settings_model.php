<?php
class Settings_model extends CI_Model {

	public function banner($lang_id="TH", array $data = array(), $limit = 1000)
	{
		$this->db->select('banner_content.*, banner_id.*');
		$this->db->from('banner_id', 'banner_content');
		
		$this->db->join("banner_content","banner_content.main_id = banner_id.main_id", "left");

		if($data) {
			foreach ($data as $key => $value) {
				$this->db->where("banner_content.".$key, $value);
			}
		}

		$this->db->where("banner_content.lang_id", $lang_id);
		$this->db->where("banner_id.main_status", "active");
		$this->db->where("banner_content.content_status", "active");
		$this->db->order_by("banner_id.sequence", "asc");
		$this->db->limit($limit);

		return $this->db->get();
	}

	public function banner_attachment($lang_id="TH", $limit = 1000)
	{
		$this->db->select('banner_attachment.*');
		$this->db->from('banner_attachment');

		$this->db->where("banner_attachment.attachment_status", "active");
		$this->db->order_by("banner_attachment.sequence", "ASC");
		$this->db->limit($limit);

		return $this->db->get();
	}

	public function address($lang_id="TH")
	{
		$this->db->select('*'); 
		$this->db->from('address_id');
		
		$this->db->join("address_content","address_content.address_id = address_id.address_id");
		$this->db->where("address_id.address_status","active");	
		$this->db->where("address_content.content_status","active");	
		$this->db->where("address_content.lang_id",$lang_id);
		$this->db->order_by("address_id.address_id","desc");
		$this->db->limit(2);

		return $this->db->get();
	}

	public function emailAdmin($email_id=NULL)
	{
		$this->db->select('*'); 
		$this->db->from('setting_email_id');
		
		$this->db->join("setting_email_content","setting_email_content.email_id = setting_email_id.email_id");

		if($email_id){
			$this->db->where("setting_email_content.content_id", $email_id);	
		}

		$this->db->where("setting_email_id.email_status","active");	
		$this->db->where("setting_email_content.content_status","active");	
		$this->db->limit(1);

		return $this->db->get()->row_array();
	}

	public function getAnalytic($url)
	{
		$this->db->where("seo_name", $url);
		$result = $this->db->get('system_seo')->row_array();

		return (!empty($result) ? $result : null);
	}

	public function getLanguageList()
	{
		$result = $this->db->where('is_deleted', 0)->get("ml_languages")->result_array();
		$language = [];
		foreach ($result as $key => $value) {
			
			$language[$key]['lang_name'] 	= $value['name'];
			$language[$key]['lang_flag'] 	= $value['language_code'].".png";
			$language[$key]['lang_id'] 		= $value['language_code'];
		}

		return $language;
	}

	public function getLanguagename($lang_id)
	{
		$this->db->where("language_code",$lang_id);
		$lang = $this->db->where('is_deleted', 0)->get("ml_languages")->row_array();

		$language['lang_name'] 	= $lang['name'];
		$language['lang_flag'] 	= $lang['language_code'].".png";
		$language['lang_id'] 	= $lang['language_code'];

		return $language['lang_name'];
	}

	public function getLanguageflag($lang_id)
	{
		$this->db->where("language_code",$lang_id);
		$lang = $this->db->where('is_deleted', 0)->get("ml_languages")->row_array();

		$language['lang_name'] 	= $lang['name'];
		$language['lang_flag'] 	= $lang['language_code'].".png";
		$language['lang_id'] 	= $lang['language_code'];

		return $language['lang_flag'];
	}
}