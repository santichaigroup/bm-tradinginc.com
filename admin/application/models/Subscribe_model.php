<?php
class Subscribe_model extends CI_Model{

	public function dataTable()
	{
		$this->db->select("*");	
		$this->db->order_by("id","desc");
		$this->db->limit(1000);
		return $this->db->get("address_contactus");
	}

	public function dataTableFaq()
	{
		$this->db->select("*");	
		$this->db->order_by("faq_id","desc");
		$this->db->limit(1000);
		return $this->db->get("address_subscribe");
	}

	public function getDetail($faq_id)
	{
		$this->db->select("address_contactus.*, setting_email_content.content_keyword AS email_keyword");
		$this->db->join("setting_email_content","address_contactus.contactus_title=setting_email_content.content_id", "left");
		$this->db->where("address_contactus.id",$faq_id);

		return $this->db->get("address_contactus")->row_array();
	}

	public function dataTableNotRead()
	{
		$this->db->select("*");
		$this->db->where("status", "new");	
		return $this->db->count_all_results("address_contactus");
	}
	
	function updateStatus($data,$faq_id)
	{
		$this->db->where("id",$faq_id);
		return $this->db->update("address_contactus",$data);
	}

	public function delete($faq_id)
	{	
		$this->db->where("id",$faq_id);	
		if($this->db->delete("address_contactus")){
			return 	true;	
		}else{
			return false;
		}
	}

	public function deleteFaq($faq_id)
	{	
		$this->db->where("faq_id",$faq_id);	
		if($this->db->delete("address_subscribe")){
			return 	true;	
		}else{
			return false;
		}	
	}

	public function category_name($category_id)
	{
		$this->db->select("*");
		$this->db->where("content_id",$category_id);
		return $this->db->get("setting_email_content")->row_array();
	}

	public function sub_category_name($category_id, $category_sub_id)
	{
		$this->db->select("*");
		$this->db->where("content_id",$category_id);
		$result = $this->db->get("setting_email_content")->row_array();

		$sub_category = explode(',', $result['content_sub_category']);

		return $sub_category[$category_sub_id];
	}
}