<?php
class Address_model extends CI_Model{
	public function dataTable()
	{//
		$this->db->select('*'); 
		$this->db->from('address_id', 'address_content');
		
		$this->db->join("address_content","address_content.address_id = address_id.address_id AND address_content.content_id = address_id.default_content_id");
		$this->db->where("address_id.address_status <>","deleted");	
		$this->db->order_by("address_id.address_id","desc");
		$this->db->limit(1000);
		return $this->db->get();
	}
	public function checkExistst($address_id)
	{
		$this->db->where("address_id.address_id",$address_id);
		$this->db->where("address_id.address_status <>","deleted");
		return $this->db->count_all_results("address_id");
	}
	public function checkdataTable()
	{
		$this->db->where("type_id",'1');
		return $this->db->get("address_id");
	}
	public function getDetail($address_id,$lang_id=NULL)
	{
		$this->db->select('*');
		$this->db->from('address_id', 'address_content');
		
		if($lang_id){
			$this->db->join("address_content","address_content.address_id = address_id.address_id AND address_content.lang_id = '".$lang_id."'");
		}else{
			$this->db->join("address_content","address_content.address_id = address_id.address_id AND address_content.content_id = address_id.default_content_id");
		}
		
		$this->db->where("address_content.lang_id",$lang_id);
		$this->db->where("address_id.address_id",$address_id);
		$this->db->where("address_id.address_status <>",'deleted');
		$this->db->order_by("address_id.address_id","desc");
		$this->db->limit(1000);
		return  $this->db->get()->row_array();
						
	}
	public function addData()
	{
		$this->db->set("type_id","1");
		$this->db->set("post_date","NOW()",false);
		$this->db->set("post_ip",$this->input->ip_address());
		$this->db->set("post_by",$this->admin_library->userdata('user_id'));
		$this->db->insert("address_id");
		$address_id = $this->db->insert_id();
		if(!$address_id){
			show_error("Cannot create  address id");	
		}
		return $address_id;
	}
	public function setDate($address_id,$address_date)
	{
		$address_date = date("Y-m-d",strtotime($address_date));
		$this->db->set("address_date",$address_date);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("address_id",$address_id);
		return $this->db->update("address_id");	
	}
	public function setStatus($address_id,$status)
	{
		$this->db->set("address_status",$status);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("address_id",$address_id);
		return $this->db->update("address_id");	
	}
	public function setDefaultContent($address_id,$content_id)
	{
		$this->db->set("default_content_id",$content_id);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("address_id",$address_id);
		return $this->db->update("address_id");
	}
	public function addLanguage($address_id,$lang_id)
	{//
		$this->db->where("address_id",$address_id);
		$this->db->where("lang_id",$lang_id);
		$has = $this->db->get("address_content");
		
		if($has->num_rows() == 0){
			$this->db->set("address_id",$address_id);
			$this->db->set("lang_id",$lang_id);
			$this->db->insert("address_content");
			return $this->db->insert_id();
		}else{
			$r= $has->row_array();
			return 	@$r['content_id'];
		}
		
	}
	function updateContent(
			$address_id,
			$lang_id,
			$subject,
			$group,
			$address,
			$phone,
			$fax,
			$email,
			$content_googlemap,$content_thumbnail,$content_detail,
			$content_facebook,$content_instagram,$content_google,$content_youtube,$content_twitter,$content_tumblr,$content_pinterest,$content_line,$content_qr_code)
	{
		$content_detail = html_entity_decode($content_detail);

		if($content_thumbnail){
			$this->db->set("content_thumbnail",$content_thumbnail);
		}

		$this->db->set("content_detail",$content_detail);
		$this->db->set("content_subject",$subject);
		$this->db->set("content_group",$group);
		$this->db->set("content_address",$address);
		$this->db->set("content_phone",$phone);
		$this->db->set("content_fax",$fax);
		$this->db->set("content_email",$email);
		$this->db->set("content_googlemap",$content_googlemap);
		$this->db->set("content_facebook",$content_facebook);
		$this->db->set("content_instagram",$content_instagram);
		$this->db->set("content_google",$content_google);
		$this->db->set("content_youtube",$content_youtube);
		$this->db->set("content_twitter",$content_twitter);
		$this->db->set("content_tumblr",$content_tumblr);
		$this->db->set("content_pinterest",$content_pinterest);
		$this->db->set("content_line",$content_line);
		$this->db->set("content_qr_code",$content_qr_code);
		
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("address_id",$address_id);
		$this->db->where("lang_id",$lang_id);
		return $this->db->update("address_content");
	}
	function updateContentStatus($address_id,$lang_id,$status)
	{
		$this->db->set("content_status",$status);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("address_id",$address_id);
		$this->db->where("lang_id",$lang_id);
		return $this->db->update("address_content");
	}
}