<style type="text/css">



</style>

<div class="box">
  <div class="box-header with-border">
    <div class="pull-left">
      <div class="btn-group">
        <button type="button" class="btn btn-default" data-toggle="dropdown">
          <i class="icon-user"></i> Tools (<span class="select_count">0</span>) <span class="icon-caret-down"></span>
        </button>
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
          <span class="caret"></span>
          <span class="sr-only">Toggle Dropdown</span>
        </button>

        <ul class="dropdown-menu">
          <li><a href="javascript:;" onclick="selectdata_delete();">ลบข้อมูล</a></li>
          <li><a href="javascript:;" onclick="selectdata_unsuspend();">แสดงข้อมูล</a></li>
          <li><a href="javascript:;" onclick="selectdata_suspend();">ไม่แสดงข้อมูล</a></li>
        </ul>
      </div>
    </div>

    <div class="pull-right">
      <a href="<?php echo admin_url($_menu_link.'/useradd'); ?>" class="btn btn-success pull-right" style="width: 150px;">
        <i class="glyphicon glyphicon-plus"></i>&nbsp;   เพิ่มผู้ใช้
      </a>
    </div>
  </div>
  <div class="box-body">

    <!--  Error Alert  -->
    <h4></h4>
    <?php if(@$success_message!=NULL){ ?>
    <div class="alert alert-success"> 
      <button class="close" data-dismiss="alert">×</button>
      <strong>Success !</strong> <?php echo $success_message; ?>
    </div>
    <?php } ?>

    <?php echo form_open('', 'name="usergroup_listform" id="usergroup_listform"'); ?>
      <input type="hidden" name="lang_id" id="lang_id" value="<?php echo $lang_id; ?>">
      <table id="datatable_list" class="table table-bordered table-striped" cellspacing="0">

        <thead>
          <tr class="info">
            <th align="center" width="5"><input type="checkbox" class="select_all_item" style="margin-left:20px;" /></th>
            <th><i class="glyphicon glyphicon-list"></i> Photo</th>
            <th><i class="glyphicon glyphicon-list-alt"></i> Username</th>
            <th><i class="glyphicon glyphicon-list-alt"></i> Fullname</th>
            <th><i class="glyphicon glyphicon-list-alt"></i> Email</th>
            <th><i class="glyphicon glyphicon-calendar"></i> Join Date</th>
            <th><i class="glyphicon glyphicon-check"></i> Status</th>
            <th class="dt-body-center"><i class="glyphicon glyphicon-wrench"></i> Actions</th>
          </tr>
        </thead>

        <tbody></tbody>

      </table>
    <?php echo form_close(); ?>

  </div>
</div>

<script>
  $(function () {

    var table = $('#datatable_list').DataTable({

          "bProcessing": true,
          "sAjaxSource"      : "<?php echo admin_url($_menu_link."/load_datatable"); ?>",
          "sAjaxDataProp"    : "aData",
          "language"  : {
            "sProcessing":   "กำลังดำเนินการ...",
            "sLengthMenu":   "แสดง _MENU_ แถว",
            "sZeroRecords":  "ไม่พบข้อมูล",
            "sInfo":         "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
            "sInfoEmpty":    "แสดง 0 ถึง 0 จาก 0 แถว",
            "sInfoFiltered": "(กรองข้อมูล _MAX_ ทุกแถว)",
            "sInfoPostFix":  "",
            "sSearch":       "ค้นหา: ",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "เิริ่มต้น",
                "sPrevious": "ก่อนหน้า ",
                "sNext":     " ถัดไป",
                "sLast":     "สุดท้าย"
            }
          },
          //  Set title Table.
          "aoColumns":[
                    {"mDataProp": "user_id" },
                    {"mDataProp": "user_thumbnail"},
                    {"mDataProp": "username"},
                    {"mDataProp": "user_fullname"},
                    {"mDataProp": "user_email"},
                    {"mDataProp": "user_joindate"},
                    {"mDataProp": "user_status"},
                    {"mDataProp": "user_id"}
          ],
          'columnDefs': [{
               'targets': 0,
               'searchable':false,
               'orderable':false,
               'className':'dt-body-center',
               'render': function (data, type, full, meta) {
                   return '<input type="checkbox" name="user_id[]" value="' 
                      + $('<div/>').text(data).html() + '">';
               }
            },
            {
                'targets': 1,
                'className':'dt-body-center',
                'render': function (data, type, full, meta) {

                  if(data) {

                    thumbnail = "<img src='uploads/system_users/images/"+data+"' class='img-circle' style='height: 70px; width: 70px;' />";

                  } else {

                    thumbnail = "<img src='images/thumbnail-default.jpg' class='img-circle' style='height: 70px; width: 70px;' />";
                  }

                  return thumbnail;
               }
            },
            {
              'targets': [2,3,4,5],
              'className':'dt-body-center'
            },
            {
                'targets': 6,
                'className':'dt-body-center',
                'render': function (data, type, full, meta) {

                  switch(data) {
                      case "active" :
                      color = "green";
                      data = "แสดงข้อมูล";
                      break;
                      case "pending" :
                      color = "orange";
                      data = "ไม่แสดงข้อมูล";
                      break;
                      case "deleted" :
                      color = "red";
                      data = "ลบข้อมูล";
                      break;
                  }

                  return "<font color='"+color+"'>"+data+"</font>";
               }
            },
            {
               'targets': 7,
               'searchable':false,
               'orderable':false,
               'className':'dt-body-center',
               'render': function (data, type, full, meta) {
                   return '<a href="<?php echo admin_url($_menu_link."/useredit/"); ?>' 
                      + $('<div/>').text(data).html() + '" class="btn btn-warning"><i class="glyphicon glyphicon-edit"></i> Edit</a>'
                      + ' <a onclick="delete_data('+ $('<div/>').text(data).html() +');" class="btn btn-danger btn-delete-row"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
            }
          }]
       });

      // ---------- Tools Select All ---------- //
        $('.select_all_item').on('click', function() {

            var rows = table.rows({ 'search': 'applied' }).nodes();
            $('input[name="user_id[]"]', rows).prop('checked', this.checked);

            var select_length = $(rows).find('input[name="user_id[]"]:checked').length;
            $(".select_count").text(select_length);

            if(!this.checked) {
              $(rows).find(".btn-delete-row").removeClass('disabled');
              $('.dataTables_paginate').find(".paginate_button ").removeClass('disabled');
            } else {
              $(rows).find(".btn-delete-row").addClass('disabled');
              $('.dataTables_paginate').find(".paginate_button ").addClass('disabled');
            }
        });

        $('#datatable_list_wrapper tbody').on('change', 'input[name="user_id[]"]', function() {

          var rows = table.rows({ 'search': 'applied' }).nodes();
          var select_length = $(rows).find('input[name="user_id[]"]:checked').length;
          $(".select_count").text(select_length);

          if(select_length > 0) {
            $(rows).find(".btn-delete-row").addClass('disabled');
          } else {
            $(rows).find(".btn-delete-row").removeClass('disabled');
            $(".select_all_item").prop('indeterminate', false).prop('checked', false);
          }

          if(!this.checked){

             var el = $('.select_all_item').get(0);
             if(el && el.checked && ('indeterminate' in el)) {

                el.indeterminate = true;
             }
          }
        });

  });

  function delete_data(news_media_id)
  {
    if(confirm("Delete Data !. Are you sure ?")){
    $("#usergroup_listform").attr("action","<?php echo admin_url(); ?>/<?php echo $_menu_link; ?>/userdelete_user/"+news_media_id);
    $("#usergroup_listform").submit();
    }
  }

  function selectdata_delete()
  {
    if(confirm("Delete Data !. Are you sure ?")){
    $("#usergroup_listform").attr("action","<?php echo admin_url(); ?>/<?php echo $_menu_link; ?>/userhandle_delete");
    $("#usergroup_listform").submit();
    }
  }

  function selectdata_suspend()
  {
    $("#usergroup_listform").attr("action","<?php echo admin_url(); ?>/<?php echo $_menu_link; ?>/userhandle_suspend");
    $("#usergroup_listform").submit();
  }

  function selectdata_unsuspend()
  {
    $("#usergroup_listform").attr("action","<?php echo admin_url(); ?>/<?php echo $_menu_link; ?>/userhandle_unsuspend");
    $("#usergroup_listform").submit();
  }
</script>