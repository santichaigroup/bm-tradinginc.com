<div class="row">

  <form name="optionform" id="optionform" method="post" enctype="multipart/form-data" role="form">
  <div class="col-md-9">

    <div class="box box-primary">
      <div class="box-header with-border toggle-click">

          <i class="glyphicon glyphicon-edit"></i>
          <h3 class="box-title">Form Box</h3>

      </div>
      <div class="box-body">

          <!--  Error Alert  -->
          <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
            <div class="alert alert-error">
                  <button class="close" data-dismiss="alert">×</button>
                  <strong>Error !</strong> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
              </div>
          <?php }?>
          <!--  Error Alert  -->

			<div class="box-body table-responsive no-padding">
			  	<table class="table table-hover">
					    <tbody>
					    <tr>
					      <th>PRODUCT</th>
					      <th>UNIT</th>
					      <th>COST</th>
					      <th>TRACKING NO.</th>
					      <th>ORDER DATE TIME</th>
					      <th>STATUS</th>
					    </tr>
					<?php
					foreach($row as $value) {

						$product 	= $value['item_name'].' '.$value['item_sku'];
						$product_link = $admin_url.'/product/edit/'.$value['item_id'].'/TH';
						$unit 		= $value['order_quantity'];
						$cost 		= $value['order_price'];
						$tracking 	= $value['shipping_tracking'];
						$order_date = $value['order_datetime'];

						switch ($value['order_status']) {
							case 'pending_payment':
								$status = "รอการชำระเงิน";
								break;
							case 'paid':
								$status = "ชำระเงินเรียบร้อย";
								break;
							case 'packing':
								$status = "บรรจุสินค้า";
								break;
							case 'shippi':
								$status = "กำลังจัดส่ง";
								break;
							default:
								$status = "เสร็จสิ้น";
								break;
						}

					?>
					    <tr>
					      <td><a href="<?php echo $product_link; ?>" target="_blank"><?php echo $product; ?></a></td>
					      <td><?php echo $unit; ?></td>
					      <td><?php echo $cost; ?></td>
					      <td><?php echo $tracking; ?></td>
					      <td><?php echo $order_date; ?></td>
					      <td><?php echo $status; ?></td>
					    </tr>
					<?php
					}
					?>
			  		</tbody>
			  	</table>
			</div>

      </div>
    </div>

  </div>

<?php
/************************************************** Tools Box **************************************************/
?>

  <div class="col-md-3">

    <div class="box box-success">
      <div class="box-header with-border">

          <i class="glyphicon glyphicon-check"></i>
          <h3 class="box-title">Tools Box</h3>

      </div>
      <div class="box-footer">
          <button type="submit" class="btn btn-success pull-right" onclick="save_form();">Export Excel</button>
          <a href="<?php echo admin_url(); ?>/<?php echo $_menu_link; ?>/index" class="btn btn-danger">ย้อนกลับ</a>
      </div>
    </div>

  </div>
  </form>

</div>

<script type="text/javascript">
  function save_form()
  {
    $("form#optionform").submit();
  }
</script>