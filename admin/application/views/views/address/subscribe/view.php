<div class="span12"> 
  <!-- BEGIN RECENT ORDERS PORTLET-->
  <div class="widget">
    <div class="widget-title widget-user">
      <h4><i class="icon-list-alt"></i> ข้อมูลผู้ติดต่อ</h4> 
      <span class="tools">
      
      </span>
      </div>
      <div class="widget-body">
		<table class="table table-striped table-bordered table-advance table-hover">
			<thead>
				<tr>
					<th width="150"><i class="icon-briefcase"></i> <span class="hidden-phone">List</span></th>
					<th><i class="icon-user"></i> <span class="hidden-phone">Data</span></th>
				</tr>
			</thead>
            <tbody>
				<tr>
					<td class="highlight" width="150">
						<div class="success"></div>
						<span class="text-list">วันที่ส่ง :</span>
					</td>
					<td><?php echo $row['create_date'];?></td>
				</tr>
				<tr>
					<td class="highlight">
						<div class="success"></div>
						<span class="text-list">Interest :</span>
					</td>
					<td>
						<?php
							echo $category_name;
						?>
					</td>
				</tr>
				<?php
				if($sub_category_name) {
				?>
				<tr>
					<td class="highlight">
						<div class="success"></div>
						<span class="text-list"></span>
					</td>
					<td>
						<?php
							echo $sub_category_name;
						?>
					</td>
				</tr>
				<?php
				}
				?>
				<tr>
					<td class="highlight">
						<div class="success"></div>
						<span class="text-list">ชื่อ-นามสกุล :</span>
					</td>
					<td><?php echo $row['contactus_fullname'];?></td>
				</tr>
				<tr>
					<td class="highlight" width="150">
						<div class="success"></div>
						<span class="text-list">ชื่อบริษัท :</span>
					</td>
					<td><?php echo $row['contactus_company'];?></td>
				</tr>
				<tr>
					<td class="highlight" width="150">
						<div class="success"></div>
						<span class="text-list">ที่อยู่ :</span>
					</td>
					<td><?php echo $row['contactus_address'];?></td>
				</tr>
                <tr>
					<td class="highlight" width="150">
						<div class="success"></div>
						<span class="text-list">อีเมล์ :</span>
					</td>
					<td><?php echo $row['contactus_email'];?></td>
				</tr>
                <tr>
					<td class="highlight" width="150">
						<div class="success"></div>
						<span class="text-list">เบอร์โทรศัพท์ :</span>
					</td>
					<td><?php echo $row['contactus_phone'];?></td>
				</tr>
				<tr>
					<td class="highlight" width="150">
						<div class="success"></div>
						<span class="text-list">จำนวน :</span>
					</td>
					<td><?php echo $row['contactus_number'];?></td>
				</tr>
                
                <tr>
					<td class="highlight" width="150">
						<div class="success"></div>
						<span class="text-list">รายละเอียด :</span>
					</td>
					<td><?php echo $row['contactus_detail'];?></td>
				</tr>
<?php

if($row['contactus_file']) {

?>
				<tr>
					<td class="highlight" width="150">
						<div class="success"></div>
						<span class="text-list">ไฟล์แนบ :</span>
					</td>
					<td><a href="<?php echo base_url(); ?>public/uploads/contactus/<?php echo $row['contactus_file'];?>" target="_blank">คลิกเพื่อดู</a></td>
				</tr>
<?php
}
?>
			</tbody>
		</table>
      </div>
		<div class="form-actions">
     	 	<button type="button" class="btn btn-info" onclick="$.getLocation('<?php echo admin_url('contact/subscribe/'); ?>');"> Back</button>   
   		</div>				
  </div>
  <!-- END RECENT ORDERS PORTLET--> 
</div> 