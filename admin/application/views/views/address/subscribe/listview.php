<style type="text/css">
  


</style>

<div class="box">
  <div class="box-header">

      <!--  Error Alert  -->
      <h4></h4>
      <?php if(@$success_message!=NULL){ ?>
      <div class="alert alert-success"> 
        <button class="close" data-dismiss="alert">×</button>
        <strong>Success !</strong> <?php echo $success_message; ?>
      </div>
      <?php } ?>

      <div class="btn-group">
         
         <button type="button" class="btn btn-default" data-toggle="dropdown"><i class="icon-user"></i> เครื่องมือ (<span class="select_count">0</span>) <span class="icon-caret-down"></span></button>
         <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
          <span class="caret"></span>
          <span class="sr-only">Toggle Dropdown</span>
        </button>

         <ul class="dropdown-menu">
            <li><a href="javascript:;" onclick="selectdata_delete();">ลบข้อมูล</a></li>
<!--             <li><a href="javascript:;" onclick="selectdata_unsuspend();">แสดงข้อมูล</a></li>
            <li><a href="javascript:;" onclick="selectdata_suspend();">ไม่แสดงข้อมูล</a></li> -->
         </ul>
      </div>

      <!-- <a href="<?php echo admin_url(); ?>/<?php echo $_menu_link; ?>/add/TH" class="btn btn-success" style="width: 150px;"><i class="icon-plus-sign"></i> เพิ่มข้อมูล</a>  -->
      <button type="button" onclick="$.getLocation('<?php echo admin_url("contact/export_member"); ?>');" class="btn btn-success" style="margin-left: 10px; border-radius: 3px;"><i class="icon-download-alt"></i> Export Excel</button>&nbsp;

      <!-- <div class="controls btn-group" style="float: right;">
          <button class="btn dropdown-toggle" data-toggle="dropdown">
            <img src="images/flags/<?php echo $this->admin_library->getLanguageflag($lang_id); ?> ">
              <?php echo $this->admin_library->getLanguagename($lang_id); ?> 
             <span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
            <?php foreach($this->admin_library->getLanguageList() as $lang){
            if($lang_id <> $lang['lang_id']){
            ?>
              <li>
                <a href="<?php echo admin_url($this->menu['menu_link']."/index/".$lang['lang_id']); ?>">
                  <img src="images/flags/<?php echo $lang['lang_flag']; ?>">
                  &nbsp;<?php echo $lang['lang_name']; ?>
                </a>
              </li>
            <?php }} ?>
          </ul>
      </div> -->

  </div>
  <div class="box-body">

    <?php echo form_open('', 'name="usergroup_listform" id="usergroup_listform"'); ?>
      <input type="hidden" name="lang_id" id="lang_id" value="<?php echo $lang_id; ?>">
      <table id="datatable_list" class="table table-bordered table-striped" cellspacing="0">

        <thead>
          <tr class="info">
            <th align="center" width="5"><input type="checkbox" class="select_all_item" style="margin-left:20px;" /></th>
            <th><i class="glyphicon glyphicon-list-alt"></i> Send name</th>
            <th><i class="glyphicon glyphicon-calendar"></i> Create date</th>
            <th class="dt-body-center"><i class="glyphicon glyphicon-wrench"></i> Actions</th>
          </tr>
        </thead>

        <tbody></tbody>

      </table>
    <?php echo form_close(); ?>

  </div>
</div>

<script>
  $(function () {

    var table = $('#datatable_list').DataTable({

          "bProcessing": true,
          "sAjaxSource"      : "<?php echo admin_url($_menu_link."/load_datatable_subscribe/".$lang_id); ?>",
          "sAjaxDataProp"    : "aData",
          "language"  : {
            "sProcessing":   "กำลังดำเนินการ...",
            "sLengthMenu":   "แสดง _MENU_ แถว",
            "sZeroRecords":  "ไม่พบข้อมูล",
            "sInfo":         "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
            "sInfoEmpty":    "แสดง 0 ถึง 0 จาก 0 แถว",
            "sInfoFiltered": "(กรองข้อมูล _MAX_ ทุกแถว)",
            "sInfoPostFix":  "",
            "sSearch":       "ค้นหา: ",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "เิริ่มต้น",
                "sPrevious": "ก่อนหน้า ",
                "sNext":     " ถัดไป",
                "sLast":     "สุดท้าย"
            }
          },
          //  Set title Table.
          "aoColumns":[
                    {"mDataProp": "faq_id" },
                    {"mDataProp": "faq_email",  "sTitle": "อีเมล"},
                    {"mDataProp": "create_date",        "sTitle": "ส่งเมื่อ"},
                    {"mDataProp": "faq_id", "sTitle":"เครื่องมือ" }
          ],
          'columnDefs': [{
               'targets': 0,
               'searchable':false,
               'orderable':false,
               'className':'dt-body-center',
               'render': function (data, type, full, meta) {
                   return '<input type="checkbox" name="main_id[]" value="' 
                      + $('<div/>').text(data).html() + '">';
               }
            },
            // Show image or icon.
            // {
            //     'targets': 3,
            //     'className':'dt-body-center',
            //     'render': function (data, type, full, meta) {

            //       return "<img data-src='holder.js/100%x180' src='<?php echo $asset_url; ?>uploads/layout/"+data+"'>";
            //    }
            // },
            {
               'targets': 3,
               'searchable':false,
               'orderable':false,
               'className':'dt-body-center',
               'render': function (data, type, full, meta) {
                   return ' <a onclick="delete_data('+ $('<div/>').text(data).html() +');" class="btn btn-danger">ลบ</a>';
            }
          }],
          'order': [0, 'desc']
       });

       $('.select_all_item').on('click', function() {

          var rows = table.rows({ 'search': 'applied' }).nodes();
          $('input[type="checkbox"]', rows).prop('checked', this.checked);

          var select_length = $('#datatable_list tbody input[type="checkbox"]:checked').length;
          $(".select_count").text(select_length);          

       });

       $('#datatable_list tbody').on('change', 'input[type="checkbox"]', function() {

          var select_length = $('#datatable_list tbody input[type="checkbox"]:checked').length;
          $(".select_count").text(select_length);

          if(!this.checked){

             var el = $('.select_all_item').get(0);
             if(el && el.checked && ('indeterminate' in el)) {

                el.indeterminate = true;
             }
          }
       });

  });

  function delete_data(news_media_id)
  {
    if(confirm("Delete Data !. Are you sure ?")){
    $("#usergroup_listform").attr("action","<?php echo admin_url(); ?>/<?php echo $_menu_link; ?>/delete_subscribe_faq/"+news_media_id);
    $("#usergroup_listform").submit();
    }
  }

  function selectdata_delete()
  {
    if(confirm("Delete Data !. Are you sure ?")){
    $("#usergroup_listform").attr("action","<?php echo admin_url(); ?>/<?php echo $_menu_link; ?>/handle_delete_subscribe_faq");
    $("#usergroup_listform").submit();
    }
  }
</script>
